## Título de la Tarea:
**Desarrollo de Prototipo de Consultorio Médico Virtual con IA**

## Descripción:
Diseñar y desarrollar un prototipo de un consultorio médico virtual que permita la interacción en tiempo real mediante video streaming con un médico impulsado por inteligencia artificial. Utilizar tecnologías como Ollama, FastAPI, HTML, CSS y JavaScript.

## Requisitos:
- **Frontend**: Implementar una interfaz de usuario amigable y responsiva con HTML, CSS y JavaScript.
- **Backend**: Utilizar FastAPI para la lógica del servidor y manejar la funcionalidad de video streaming.
- **Integración de IA**: Conectar con un modelo de IA (Llama, GPT, PHI u otro) usando Ollama para realizar consultas médicas.

## Tareas:
1. **Diseño de Interfaz**: Crear y codificar la interfaz.
2. **Funcionalidad de Streaming**: Integrar video streaming en tiempo real.
3. **Integración de IA**: Implementar la IA para responder preguntas médicas.

## Entregables:
- Código fuente completo del proyecto en Git Lab.
- Reporte.

